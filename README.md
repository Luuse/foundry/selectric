# selectric

In 1961, IBM Selectric was the first typewriting machine using a sphere (frequently called a “type ball”) instead of individual typebars.
Firstly seen on a typescript facsimile of the japanese philosopher Shûzô Kuki, the rigidity of the monospace system mixed with the old style garalde serif style of the letters caught my attention.

Léonard Mabille (Luuse) 2015 - contact@luuse.io

```
OTF/
├── SelectricMono.otf
└── SelectricRoman.otf
SFD
├── SelectricMono.sfd
└── SelectricRoman.sfd
```

![img](img/01.jpg)  
![img](img/02-0.jpg)  
![img](img/02-1.jpg)  
![img](img/02-2.jpg)  
![img](img/03.jpg)  
![img](img/04.jpg)  
![img](img/05.jpg)  
![img](img/06.jpg) 
![img](img/07.jpg)  
![img](img/08.jpg)  
![img](img/09.jpg)  
![img](img/11.jpg)  
![img](img/12.gif)  



## Licence
Selectric is under [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")

---

